<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Currency;

class UpdateCurrencyRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_currency_rate:Currency_rate_updated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Currency updated as schedule based';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $get_json_data = file_get_contents("http://data.fixer.io/api/latest?access_key=bb161f16bd204311cfeace718d2d0ea7");
        // $data = json_decode($get_json_data,true);
        // $base = $data['base'];
        // return $base;

        // $code = $money = [];
        // $count = 0;
        // foreach ($data['rates'] as $key => $value) {
        // 	$code[$count] = $key;
        // 	$money[$count] = $value;
        // 	$count+=1;
        // }
        // // return $count;
        // $old_rate = Currency::all();

        // if(count($old_rate) != null){
        //     $old_rate = Currency::truncate();

        //     for ($i=0; $i < $count; $i++) { 
        //         $crncy = new Currency();
        //         $crncy->base = $base;
        //         $crncy->currency_code = $code[$i];
        //         $crncy->currency_rate = $money[$i];
        //         $crncy->save();
        //     }
        // }else{
        //     for ($i=0; $i < $count; $i++) { 
        //         $crncy = new Currency();
        //         $crncy->base = $base;
        //         $crncy->currency_code = $code[$i];
        //         $crncy->currency_rate = $money[$i];
        //         $crncy->save();
        //     }
        // }

        $this->info('currency updated');
    }
}
