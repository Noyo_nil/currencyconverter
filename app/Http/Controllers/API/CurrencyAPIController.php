<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Http\Controllers\Controller as Controller;
use App\Currency;
use Validator;


class CurrencyAPIController extends Controller
{
    
    public function index()
    {
        // echo "Data DIsplayed ";
        $currency = Currency::all();
        return $currency;
        // return $this->sendResponse($currency->toArray(), 'Currency list retrieved successfully.');
    }

}